# CLM Movie Challenge

## Descripción

Se construyó a través de una aplicación nodeJS con mongobd un microservicio con 3 endpoint los cuales se conectan a través de un api
a la página de películas http://www.omdbapi.com/

## Como ejecutar el proyecto

* En la carpeta principal escribir en consola: `docker-compose up`
* El proyecto se ejecutará en localhost:3000

# Como Usar

* Para la prueba de los endpoint se utiliza postman

* 1.- Get Movie

Se recibe el titulo de una película o el nombre de la película y el año para luego ser buscada por el api de ombd y en caso de encontrarla la almacena en la base de datos.

![img!](img/clm-getmovie.PNG)

* 2.- List movie

Se muestra un listado de las películas almacenadas ordenadas alfabéticamente de una página especificada.

![img!](img/clm-listMovie.PNG)


* 3.- Update Movie

Se elección el plot de una película para luego elegir una palabra y reemplazarla con otra.

![img!](img/clm-updateMovie.PNG)
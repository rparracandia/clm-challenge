const Joi = require('joi')

const movieSchemaParams = Joi.object().keys({
    
    title: Joi.string().required(),

})

const movieSchemaHeaders = Joi.object().keys({

    year: Joi.string().min(4)


})

const pageValidator = Joi.object().keys({

    page : Joi.number().min(0).required()

})

const replaceValidathor = Joi.object().keys({

    title : Joi.string().required(),
    find: Joi.string().required(),
    replace: Joi.string().required()

})

const movieValidatorParams = async (ctx,next)=>{
    
    let {error}=movieSchemaParams.validate({
        title: ctx.query.title,
    })
    if (error) {
        ctx.status = 400
        ctx.body = {
          success: false,
          info: 'Invalid input',
          details: error
        }
        return}
    return next()
}

const movieValidatorheaders = async (ctx,next)=>{
    
    let {error}=movieSchemaHeaders.validate({
        year: ctx.headers.year})
    if (error) {
        ctx.status = 400
        ctx.body = {
          success: false,
          info: 'Invalid input',
          details: error
        }
        return}
    return next()
}

const listMoviesValidator= async(ctx,next) => {

    let {error}=pageValidator.validate({
        page: ctx.headers.page})
    if (error) {
        ctx.status = 400
        ctx.body = {
          success: false,
          info: 'Invalid input',
          details: error
        }
        return}
    return next()

}

const RepalcePlotValidator= async(ctx,next) => {

    let {error}=replaceValidathor.validate({
        title: ctx.request.body.title,
        find: ctx.request.body.find,
        replace: ctx.request.body.replace})
    if (error) {
        ctx.status = 400
        ctx.body = {
          success: false,
          info: 'Invalid input',
          details: error
        }
        return}
    return next()

}



module.exports = {movieValidatorParams ,
                 movieValidatorheaders,
                 listMoviesValidator,
                 RepalcePlotValidator


}
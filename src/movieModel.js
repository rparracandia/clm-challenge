const { Schema, model } = require('mongoose')
const database = {
    URI: 'mongodb://mongo/moviesdb',
    collectionName: 'movie'
}


const movieSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique : true
    },
    year: {
        type: String,
        required: true,
    },
    released: {
        type: String,
        required: true,
    },
    genre: {
        type: String,
        required: true,
    },
    directors:{
        type: String,
        required: true,
    },
    actors:{
        type: String,
        required: true,
    },
    plot:{
        type: String,
        required: true,
    },
    ratings: [new Schema({
        Source:{
            type: String,
            required: false,
         },
         Value:{
             type: String,
             required: false,
         }
    }, { _id : false })]


})
module.exports = model('movie', movieSchema, database.collectionName )


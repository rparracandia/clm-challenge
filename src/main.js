const Koa = require('koa')
const koaRouter = require('koa-router')
const axios = require('axios')
const movieSchema = require('./movieModel')
const {movieValidatorParams,movieValidatorheaders,listMoviesValidator,RepalcePlotValidator} = require('./middleware')
const json = require('koa-json')
const bodyParser = require('koa-bodyparser')


const externalAPI={
  host: 'https://www.omdbapi.com/',
  API_KEY: '7e4a07ca'
}

require('./database')

const app = new Koa()
const router = new koaRouter()


router.get('/get-movie',movieValidatorParams,movieValidatorheaders ,async (ctx) => {
  
  ctx.headers.year ? {data,status} = await axios.get(`https://www.omdbapi.com/?apikey=${externalAPI.API_KEY}&t=${ctx.query.title}&y=${ctx.headers.year}`) : 
  {data,status} = await axios.get(`https://www.omdbapi.com/?apikey=${externalAPI.API_KEY}&t=${ctx.query.title}`)

  if(status == 200) {
  
    let movie = new movieSchema({
        title: data.Title,
        year: data.Year,
        released: data.Released,
        genre: data.Genre,
        directors: data.Director,
        actors: data.Actors,
        plot: data.Plot,
        ratings: data.Ratings
      })
      try {
        await movie.save()
        ctx.status = 200
        ctx.body = movie
      }
      catch(err) {
        ctx.status = 303
        ctx.body = 'The movie is already stored'
        
      }
    }})


router.get('/list-movie',listMoviesValidator,async (ctx) => {

  let page= ctx.headers.page

  let listMovies = await movieSchema.find({}).sort({title : 1}).skip(5*page).limit(5).exec()
  console.log(listMovies)

  ctx.body =listMovies

})


router.post('/update-movie',RepalcePlotValidator, async (ctx) => {
  
  
  let {title,find,replace} = ctx.request.body

  let movie = await movieSchema.findOne({title}).exec()
  
  if(movie){
    movie.plot = movie.plot.replace(find,replace)
    console.log(movie.plot)
    movieSchema.findOneAndUpdate({_id:movie._id},{plot:movie.plot}).exec()

    ctx.status = 200
    ctx.body = movie
    return
  }
  ctx.status = 404
  ctx.body = 'Movie dont exist in the data base'

  
})


app.use(json())
.use(bodyParser())
.use(router.routes())
.use(router.allowedMethods())
.listen(3000, () =>{
     console.log('Aplication is running')
})
FROM node:16.13.1-alpine3.15


RUN mkdir /app
WORKDIR /app

COPY src/main.js ./
COPY  src/package.json ./

RUN npm install
CMD [ "node", "main.js" ]
EXPOSE 3000

